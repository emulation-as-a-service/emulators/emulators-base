ARG base=ubuntu:20.04

FROM "$base" AS build

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    autotools-dev \
    build-essential \
    debhelper \
    dh-autoreconf \
    dpkg-dev \
    libasound2-dev \
    libaudiofile-dev \
    libcaca-dev \
    libcairo-dev \
    libcunit1-dev \
    libgl1-mesa-dev \
    libglu1-mesa-dev \
    libossp-uuid-dev \
    libpango1.0-dev \
    libpulse-dev \
    libpulse0 \
    libvncserver-dev \
    libvorbis-dev \
    libx11-6 \
    libx11-dev \
    libxext-dev \
    libxext6 \
    libxkbcommon-dev \
    libxkbcommon0 \
    libxt-dev \
    libxv-dev \
    nasm \
    x11proto-core-dev \
    && :

WORKDIR /guacamole-server-eaas
COPY guacamole-server-eaas .
RUN autoreconf -i
RUN ./configure --without-pulse --without-rdp --enable-pipeaudio \
    --disable-ssh-agent --without-vnc
RUN make install

WORKDIR /libsdl-eaas
COPY libsdl-eaas .
RUN ./autogen.sh
RUN ./configure --enable-static=no --disable-rpath \
    --enable-sdl-dlopen --disable-loadso \
    --disable-video-ggi --disable-video-svga --disable-video-aalib \
    --disable-nas --disable-esd --disable-arts \
    --disable-alsa-shared \
    --enable-video-sdlonp --enable-nasm
RUN make install

WORKDIR /fake-clock
COPY fake-clock .
RUN make
RUN mkdir -p /usr/local/lib && cp LD_PRELOAD_clock_gettime.so /usr/local/lib

###############################################################################

FROM "$base"
COPY --from=build /usr/local/ /usr/local/

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y ca-certificates curl \
    && rm -rf /var/lib/apt/lists/* \
    && . /etc/os-release \
    && curl -fL https://xpra.org/gpg.asc -o /usr/share/keyrings/xpra.asc \
    && curl -fL "https://raw.githubusercontent.com/Xpra-org/xpra/HEAD/packaging/repos/$VERSION_CODENAME/xpra.sources" -o /etc/apt/sources.list.d/xpra.sources

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    xpra pulseaudio vde2 socat x11-xserver-utils libossp-uuid16 \
    patch xinput \
    && rm -rf /var/lib/apt/lists/*

RUN ! ldd \
    /usr/local/lib/libSDL.so \
    /usr/local/lib/libguac-client-sdlonp.so \
    /usr/local/lib/libguac.so \
    | tee /dev/stderr | grep "not found"

COPY init /
# TODO: Rename /usr/bin/emucon-init everywhere?
RUN ln -s /init /usr/bin/emucon-init

RUN sed -i '$s/$/ -nolisten local/' /etc/xpra/conf.d/55_server_x11.conf
# TODO: x11-xserver-utils for xhost, which is probably not necessary due to the next line
RUN sed -i 's/-auth *[^ ]*//' /etc/xpra/conf.d/55_server_x11.conf

RUN useradd -m --uid 1000 --groups xpra bwfla
RUN chmod a=rwx /home/bwfla

RUN mkdir -p /run/user/1000/xpra
RUN chmod a=rwx /run/user/1000/xpra

COPY xpra.patch /usr/lib/python3/dist-packages/
RUN cd /usr/lib/python3/dist-packages && patch -p1 < xpra.patch
